/**************************************************************************//**
 * @file
 * @brief template for agro modem
 * @version 1.0.0
 ******************************************************************************
 * @section License
 * <b>Copyright 2015 Silicon Labs, Inc. http://www.silabs.com</b>
 *******************************************************************************
 *
 * This file is licensed under the Silabs License Agreement. See the file
 * "Silabs_License_Agreement.txt" for details. Before using this software for
 * any purpose, you must agree to the terms of that agreement.
 *
 ******************************************************************************/

#include <stdint.h>
#include <stdbool.h>
#include <intrinsics.h>
#include "em_device.h"
#include "em_chip.h"
#include "em_emu.h"
#include "em_cmu.h"
#include "em_gpio.h"
#include "em_rtc.h"

#include "gpio.h"
#include "rtc.h"
#include "spi.h"
#include "radio.h"
#include "uart.h"
#include "adc.h"


int32_t temperature = 45;		

/**************************************************************************//**
 * @brief Update clock and wait in EM2 for RTC tick.
 *****************************************************************************/
void clockLoop(void)
{
  //radioRXCon();
  radioTest();   // needed for entering into sleep mode, find out why!!
  gpioPULSEPWRON();
  while (1)
  {
    //EMU_EnterEM2(true);
    gpioToggleLED();
    uartSend(adcPulseRead());
/*   
    gpioTEMPPWRON();
    //temperature --;
    //if(temperature < -45)temperature = 45;
    temperature = adcTempRead();
    uartSend(0x12);
    uartSend(temperature);
    uartSend(temperature >> 8);   
    uartSend(0x13);
    gpioTEMPPWROFF();
*/   
    rtcWait(20);
    if(gpioGetSleepFlag()) //go to sleep
    {
      gpioPULSEPWROFF();
      //gpioTEMPPWROFF();
      gpioClearLED();
      radioSleep();
      RTC_IntClear(RTC_IFC_COMP0);
      RTC_IntDisable(RTC_IEN_COMP0 );
      NVIC_DisableIRQ(GPIO_ODD_IRQn);
      EMU_EnterEM2(true);
    }
    
    //   
  }
}

/**************************************************************************//**
 * @brief  Main function
 *****************************************************************************/
int main(void)
{
  /* Chip errata */
  CHIP_Init();

  /* Ensure core frequency has been updated */
  SystemCoreClockUpdate();
  
  gpioSetup();
  
  spiSetup();

  /* Setup RTC to generate an interrupt every minute */
  rtcSetup();
  
  uartInit(115200); //538uA
  
  
  adcInit();
  
  gpioSetLED();
  rtcWait(200);
  gpioClearLED();
  

  /* Main function loop */
  clockLoop();

  return 0;
}
