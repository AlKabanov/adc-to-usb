
/**************************************************************************//**
 * @brief Enables LFACLK and selects LFXO as clock source for RTC
 *        Sets up the RTC to generate an interrupt every minute.
 *****************************************************************************/
void rtcSetup(void);

/**************************************************************************//**
 * @brief delay function
 * @param [mSec] delay in mSec
 *****************************************************************************/

void rtcWait(uint32_t mSec);