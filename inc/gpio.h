



void gpioSetup(void);
void gpioSetLED(void);
void gpioClearLED(void);
void gpioToggleLED(void);

void gpioLDOOn(void);
void gpioLDOOff(void);
void gpioTEMPPWRON(void);
void gpioTEMPPWROFF(void);
void gpioPULSEPWRON(void);
void gpioPULSEPWROFF(void);

// set radio RST pin to given value (or keep floating!)
void gpioRST(uint8_t val);

// val ==1  => tx 1, rx 0 ; val == 0 => tx 0, rx 1
void gpioRXTX (uint8_t val);

// set radio NSS pin to given value
void gpioNSS (uint8_t val);

bool gpioGetSleepFlag(void);